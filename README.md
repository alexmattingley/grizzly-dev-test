# Grizzly Front End Dev Evaluation

This evaluation is meant to explore your techniques and code used in the construction of a very graphically simple website. We are looking for a faithfully styled and coded responsive realization of the included comp. Additionally, this site should be a functioning WordPress install, and we are limiting the time to 10 hours of work. If you haven't quite finished the project by the time 10 hours is up, please put your pencil down and commit your code. This certainly isn't meant to be a many-day project. All non-photo design elements should be css-generated. Please make note that attention to detail is paramount in this exercise. 

We request that you first clone down this repo to your target directory and work from there. Please build the site in a private repo on your BitBucket or GitHub account for our review. When the project is completed, we ask that you push your code up to a dev environment on your servers somewhere for public review as well. 

#### Files and languages
- The final deliverable shall be a functioning WordPress site. A simple one, albeit, but WordPress nonetheless
- The evaluation should be built using SCSS (Compass, Bourbon if you like), HTML5 and JS.
- Hipster Ipsum should be used for text content and the background image is provided in the `design-assets` folder. 
- FontAwesome should be used for social icons & the mail icon.
- The font used is all the same – Google font Ubuntu (Bold and Regular) 
- Finally, the carousel should should pull a real Instagram feed. If an API key is needed, you may use your own feed, a dummy feed or our test feed (username: **`grizzlytest`** and password: **`GZT3st`**)

# Additional Considerations:

#### WordPress Framework/Plugin Considerations
Given the time-sensitive nature of this project we encourage you to utilize the WordPress starter or framework you are most comfortable with (we love Sage by Roots.io, but are also big fans of _s and bones as well). The email signup should be a Gravity Forms embed. Our one requirement with regard to Plugins used is that no core functionality (javascript includes, css includes, font includes) be accomplished using a plugin. We expect some type of properly-managed enqueued (or gulp/grunt/codekit) inclusion. If any of this is unclear please ask questions.

#### Responsive Considerations
We are a mobile-first agency in theory, but generally this is realized in the sense of a discussion of mobile intention and code reflecting the Developer's best judgement of how to proceed. Use feedback given during initiation call.

#### Image carousel
The image carousel shown represents a manually created Instagram feed. Use the carousel plugin of your choice (ours is Owl Carousel currently), and please ensure that the carousel consists of 8 images on an endless loop, linking out to the original instagram post. 

#### Form Styling
Form should be styled as close as possible to the exact flat style shown here, not using the default browser/device styling. Hint: disable CSS styles in Gravity Forms to make your job a LOT easier. Double-Hint: we have been known to use Formalize.me for keeping baseline styles as consistent as possible across browsers and devices

# Evaluatory Criteria 

#### Code == Design
We don't move from design to development with the mindset that we are sacrificing design integrity. Development is about bringing design to life, and our developers are as integral to this process as our designers. Dev shouldn't be restrictive, and we encourage you as a developer to work with our designers and strategists to create amazing interactive experiences. 

#### Attention to Detail
This is huge. We expect that all code written for/by us has been tested, cross-browsered, and optimized. Images should be optimized, css/js should be minimal and scalable and the intent of the design should be reflected in the final deliverable.

#### Code Quality
This is another huge component of the Grizzly ethos. We will be looking through every line of submitted code, and we are going to be looking for best-practice-caliber php, javascript and HTML. Keep your code clean and comment, comment, comment. 

#### Commit Quality
We share code a lot here, and as such it's very important to commit often and to include informative, helpful commit messages. This makes everyone's job easier down the road, and it certainly makes tracking down a bug or error much much less cumbersome.

